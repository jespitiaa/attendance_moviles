package com.example.juan_.asistencia

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_absents.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import java.io.FileWriter
import java.io.IOException
import org.json.JSONObject
import java.lang.Exception
import java.net.URL
import kotlin.math.abs

class AbsentsActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    var spinner: Spinner?=null
    var showText: TextView?=null
    var dateText: TextView?=null
    private var selectedDate:String = ""
    lateinit var dates:Array<String>
    lateinit var presentStudents:JSONObject
    lateinit var totalStudents: JSONObject
    var absentStudents:ArrayList<JSONObject> = ArrayList()

    override fun onNothingSelected(parent: AdapterView<*>?) {}

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        selectedDate = dates.get(position!!)
        dateText!!.setText(selectedDate)
    }

    fun consultar(){
        doAsync {
            var lista: String = URL("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/$selectedDate/students").readText()
            uiThread {
                presentStudents = JSONObject(lista)
                if(presentStudents!!.length()==0 || !presentStudents!!.keys().hasNext()){
                    showText!!.setText("No hay documentos para $selectedDate")
                }
                else {
                    doAsync {
                        //Puede que la url esté mal pero dios sabrá
                        var lista2: String = URL("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList/").readText()
                        uiThread {
                            totalStudents = JSONObject(lista2)
                            //Tampoco sé acá cómo esté formada la colección de estudiantes
                            var arr: JSONArray = totalStudents.getJSONArray("documents")
                            if(arr != null && arr.length()>0){
                                print("arr not empty")
                                for (i in 0 until arr.length()){
                                    var actual = arr.getJSONObject(i)
                                    absentStudents.add(actual)
                                }
                                var arr2 = presentStudents.getJSONArray("documents")
                                if(arr2 != null && arr2.length()>0){
                                    print("arr not empty")
                                    for (i in 0 until arr2.length()){
                                        var actual2 = arr2.getJSONObject(i)
                                        for (k in 0 until arr.length()){
                                            //No sé si esta comparación esté bien pero la idea es buscar cuáles de los presentes están en la lista para borrarlos
                                            var metido:JSONObject = arr.getJSONObject(k)
                                            if (actual2.get("code").equals(metido.get("code"))){
                                                arr.remove(k)
                                            }
                                        }
                                    }
                                }
                            }

                            showText!!.setText("${absentStudents.toString()}")
                        }

                    }
                }
            }

        }
    }

    fun guardarCSV(){
        if(selectedDate.equals("")){
            showText!!.setText("No has elegido ninguna fecha")
        }
        else if(absentStudents.isEmpty()){
            if(totalStudents.length()==0){
                showText!!.setText("Haz clic en consultar para buscar los estudiantes ausentes")
            }
            else showText!!.setText("No hay estudiantes ausentes en esta fecha")
        }
        else{
            var fileWriter:FileWriter? = null
            try {
                fileWriter = FileWriter("absents-$selectedDate.csv")
                fileWriter!!.append("code,name")
                fileWriter!!.append('\n')
                for(abs in absentStudents){
                    //No sé si este sea el nombre. La cuota se excedió
                    fileWriter!!.append("${abs.get("code")},${abs.get("name")}")
                    fileWriter!!.append('\n')
                }
            }
            catch (e: Exception) {
                println("Writing CSV error!")
                toast("No se pudo guardar el CSV")
                e.printStackTrace()
            } finally {
                try {
                    fileWriter!!.flush()
                    fileWriter.close()
                } catch (e: IOException) {
                    println("Flushing/closing error!")
                    toast("Excepción en el cierre del canal")
                    e.printStackTrace()
                }
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_absents)
        val boton: Button = findViewById(R.id.consultar_button)
        boton.setOnClickListener { consultar() }
        val botonSub: Button = findViewById(R.id.savecsv)
        botonSub.setOnClickListener { guardarCSV() }
        dates = arrayOf("")
        showText = this.showInfo
        dateText = this.textView
        spinner = this.spinner_date
        spinner!!.setOnItemSelectedListener(this)
        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, dates)
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        spinner!!.setAdapter(aa)

    }
}
