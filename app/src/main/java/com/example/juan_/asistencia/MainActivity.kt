package com.example.juan_.asistencia

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.bluetooth.BluetoothAdapter
import android.os.Build
import android.os.Build.VERSION_CODES.*
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import org.json.JSONObject
import java.net.URL
import java.text.SimpleDateFormat

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import android.os.StrictMode
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread


class MainActivity : AppCompatActivity() {
    lateinit var currDate :String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val boton: Button = findViewById<Button>(R.id.connect_button)
        boton.setOnClickListener { connect() }
        if (Build.VERSION.SDK_INT >= O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("dd-M-yyyy")
            currDate =  current.format(formatter)
        } else {
            var date = Date();
            val formatter = SimpleDateFormat("dd-M-yyyy")
            currDate = formatter.format(date)
        }
        if (android.os.Build.VERSION.SDK_INT > 9) {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }
    }

    private fun connect() {
        val texto: TextView = findViewById(R.id.text_info)
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        print("presionado")
        var estoy = false
        print("fetching")
        doAsync {
            var lista: String = URL("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/$currDate/students").readText()
            uiThread {
                var estudiantes = JSONObject(lista)
                if(estudiantes.length()==0 || !estudiantes.keys().hasNext()){
                    texto.setText("No hay documentos para $currDate")
                }
                else {
                    texto.setText(lista)
                    var arr = estudiantes.getJSONArray("documents")
                    if(arr != null && arr.length()>0){
                        print("arr not empty")
                        for (i in 0 until arr.length()){
                            var actual = arr.getJSONObject(i).getJSONObject("fields").getJSONObject("code")
                            texto.setText(actual.toString())
                            if(actual.getString("stringValue").equals("201614195")){
                                estoy = true
                            }
                        }
                    }
                    if(!estoy){
                        if (mBluetoothAdapter == null) {
                            toast("Device does not support Bluetooth")

                        } else {
                            if (!mBluetoothAdapter.isEnabled) {
                                var bton = mBluetoothAdapter.enable()
                                if (!bton) {
                                    texto.setText("Necesitas otorgar permisos para bluetooth")
                                    toast("No tienes bluetooth encendido")
                                }
                            }
                            if (!mBluetoothAdapter.isDiscovering) {
                                var ok = mBluetoothAdapter.startDiscovery()
                                if (ok) {
                                    toast("Discovery turned on")
                                    texto.setText("Búsqueda bluetooth habilitada")
                                } else texto.setText("Necesitas otorgar permisos para discovery")
                            } else {
                                toast("Discovery already on")
                                texto.setText("Búsqueda bluetooth habilitada")
                            }
                        }
                    }
                    else{
                        texto.setText("Ya estás en la lista de asistencia de hoy, $currDate")
                    }
                }
            }

        }




    }
}
